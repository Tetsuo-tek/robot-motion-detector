#ifndef ROBOTMOTIONDETECTION_H
#define ROBOTMOTIONDETECTION_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Multimedia/Image/Detection/skmotiondetect.h>
#include <Core/System/Network/FlowNetwork/skflowvideosubscriber.h>
#include <Core/System/Network/FlowNetwork/skflowgenericpublisher.h>
#include <Core/System/Network/FlowNetwork/skflowvideopublisher.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class RobotMotionDetector extends SkFlowSat
{
    SkString srcName;
    SkMat f;

    Size blkSz;
    UInt minBlocks;
    double threshold;
    SkMotionDetect detector;

    int previewCompression;
    Size previewRes;

    double limitedTickInterval;
    SkElapsedTime chrono;

    SkFlowVideoSubscriber *subscriber;

    SkFlowGenericPublisher *canvasPublisher;
    SkFlowGenericPublisher *blocksPublisher;
    SkFlowVideoPublisher *previewPublisher;

    public:
        Constructor(RobotMotionDetector, SkFlowSat);
        Slot(onSubscriberReady);

    private:
        bool onSetup()                                              override;
        void onInit()                                               override;
        void onQuit()                                               override;

        void onFlowDataCome(SkFlowChannelData &chData)              override;

        bool hasTargets();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // ROBOTMOTIONDETECTION_H
