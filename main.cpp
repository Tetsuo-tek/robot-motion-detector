#include "robotmotiondetection.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--src-channel",           "-s", "",           "Setup the source channel");
    cli->add("--block-size",            "-b", "16x16",      "Setup the detection block-size (WxH)");
    cli->add("--min-changed-blocks",    "-m", "30",         "Setup the minimum number of blocks changed to validate detection");
    cli->add("--threshold",             "-t", "0.20",       "MSE threshold (>) to rate a block as changed");
    cli->add("--preview-resolution",    "-p", "320x240",    "Setup the preview frames resolution (WxH)");
    cli->add("--preview-compression",   "-c", "80",         "Setup the JPeg compression for preview frames [1, 100]");

    cli->add("--force-tick-time",       "-k", "0.0",         "Setup the tick detection time interval in seconds (useful to limit rate on slow CPUs)");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotMotionDetector;

    return skApp->exec();
}
