#include "robotmotiondetection.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(RobotMotionDetector, SkFlowSat)
{
    subscriber = nullptr;
    canvasPublisher = nullptr;
    blocksPublisher = nullptr;
    previewPublisher = nullptr;

    minBlocks = 0;
    threshold = 0.;

    limitedTickInterval = 0.;
    previewCompression = 0;

    setObjectName("RobotMotionDetector");

    SlotSet(onSubscriberReady);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotMotionDetector::onSetup()
{
    detector.setObjectName(this, "Detector");

    SkCli *cli = skApp->appCli();

    AssertKiller(!cli->isUsed("--src-channel"));
    srcName = cli->value("--src-channel").toString();

    minBlocks = cli->value("--min-changed-blocks").toUInt32();
    threshold =  cli->value("--threshold").toDouble();

    SkString s = cli->value("--block-size").toString();
    parseSize(objectName(), s.c_str(), &blkSz);

    s = cli->value("--preview-resolution").toString();
    parseSize(objectName(), s.c_str(), &previewRes);

    limitedTickInterval = cli->value("--force-tick-time").toDouble();
    previewCompression = cli->value("--preview-compression").toInt();

    subscriber = new SkFlowVideoSubscriber(this);
    subscriber->setObjectName(this, "Subscriber");
    subscriber->setup(&f);

    setupSubscriber(subscriber);

    Attach(subscriber, ready, this, onSubscriberReady, SkOneShotDirect);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotMotionDetector::onInit()
{
    subscriber->subscribe(srcName.c_str());
}

void RobotMotionDetector::onQuit()
{
    if (detector.isInitialized())
        detector.release();

    subscriber->close();

    if (!previewPublisher)
        return;

    previewPublisher->stop();
    canvasPublisher->stop();
    blocksPublisher->stop();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotMotionDetector, onSubscriberReady)
{
    SilentSlotArgsWarning();

    Size resolution = subscriber->resolution();
    detector.setup(resolution, blkSz, threshold);

    SkArgsMap properties;

    properties["origin"] = srcName;

    SkString s(resolution.width);
    s.append("x");
    s.concat(resolution.height);

    properties["resolution"] = s;

    if (limitedTickInterval > 0.)
    {
        properties["fps"] = 1 / limitedTickInterval;
        properties["tickTimePeriod"] = limitedTickInterval;
    }

    else
    {
        properties["fps"] = subscriber->framesPerSecond();
        properties["tickTimePeriod"] = subscriber->tickTimePeriod();
    }


    s.clear();
    s.concat(blkSz.width);
    s.append("x");
    s.concat(blkSz.height);

    properties["blockSize"] = s;
    properties["threshold"] = threshold;
    properties["totalBlocks"] = detector.total();

    canvasPublisher = new SkFlowGenericPublisher(this);
    canvasPublisher->setObjectName(this, "CanvasPublisher");
    canvasPublisher->setup("Canvas", FT_CV_OBJECT_DETECTED_BOX, T_UINT32, "", &properties);

    setupPublisher(canvasPublisher);

    blocksPublisher = new SkFlowGenericPublisher(this);
    blocksPublisher->setObjectName(this, "BlocksPublisher");
    blocksPublisher->setup("Blocks", FT_CV_OBJECT_DETECTED_BOX, T_UINT32, "", &properties);

    setupPublisher(blocksPublisher);

    previewPublisher = new SkFlowVideoPublisher(this);
    previewPublisher->setObjectName(this, "PreviewPublisher");
    previewPublisher->setup(&f, previewRes, subscriber->framesPerSecond(), previewCompression, "Preview", true);

    setupPublisher(previewPublisher);

    canvasPublisher->start();
    blocksPublisher->start();
    previewPublisher->start();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotMotionDetector::hasTargets()
{
    return canvasPublisher->hasTargets() || blocksPublisher->hasTargets() || previewPublisher->hasTargets();
}

void RobotMotionDetector::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!hasTargets())
        return;

    if (chData.chanID == subscriber->source()->chanID)
    {
        if (limitedTickInterval > 0.)
        {
            if (chrono.stop() <= limitedTickInterval)
                return;

            chrono.start();
        }

        detector.tick(f.get());

        if (detector.count() < minBlocks)
        {
            previewPublisher->tick();
            return;
        }

        SkList<Rect> &detectedBoxes = detector.getDetectedBlocks();

        UInt r[4];
        ULong rSz = sizeof(UInt)*4;

        if (previewPublisher->hasTargets() || canvasPublisher->hasTargets())
        {
            Rect &detectedCanvas = detector.getDetectedCanvas();

            Scalar color = Scalar(255, 255, 255);
            rectangle(f.get(), detectedCanvas, color, 1);

            r[0] = detectedCanvas.x;
            r[1] = detectedCanvas.y;
            r[2] = detectedCanvas.width;
            r[3] = detectedCanvas.height;

            canvasPublisher->tick(r, rSz);
        }

        if (previewPublisher->hasTargets() || blocksPublisher->hasTargets())
        {
            SkList<Rect> &detectedBoxes = detector.getDetectedBlocks();
            UInt rArraySz = detectedBoxes.count()*rSz;
            UInt rArray[rArraySz];

            Scalar color = Scalar(0, 255, 255);

            ULong counter = 0;
            SkAbstractListIterator<Rect> *itr = detectedBoxes.iterator();

            while(itr->next())
            {
                Rect &block = itr->item();
                rectangle(f.get(), block, color, 1);

                r[0] = block.x;
                r[1] = block.y;
                r[2] = block.width;
                r[3] = block.height;

                memcpy(&rArray[counter], r, rSz);
                counter++;
            }

            delete itr;

            blocksPublisher->tick(rArray, rArraySz);
        }

        previewPublisher->tick();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
